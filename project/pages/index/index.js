var _app = require("../../npm/app.js")
// console.log(_app)
class Index extends _app.Component{
  constructor() {
    super()
    this.state = {
      count:0
    }
  }
  componentDidMount(){
    console.log('执行componentDidMount')
    this.setState({
      count: 1
    });
  }
  onAddClick(){
    this.setState({
      count:this.state.count + 1
    })
  }
  onReduceClick(){
    this.setState({
      count:this.state.count -1
    })
  }
}
// console.log(Index)
Index.$$event = ['onAddClick','onReduceClick']
Page(_app.createPage(Index))