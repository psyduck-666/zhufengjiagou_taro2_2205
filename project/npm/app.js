function createPage(ComponentClass) {
  const componentInstance = new ComponentClass()
  const initData = componentInstance.state;
  const option = {
    data: initData,
    onLoad() {
      this.$component = new ComponentClass()
      this.$component._init(this)
    },
    onReady() {
      if (typeof this.$component.componentDidMount === 'function') {
        this.$component.componentDidMount()
      }
    }
  }
  const events = ComponentClass['$$event']
  console.log(this)
  if(events){
    events.forEach(eventHandlerName=>{
      if(option[eventHandlerName]) return
      option[eventHandlerName] = function (){
        this.$component[eventHandlerName].call(this.$component)
      }
    })
  }
  return option;
}

class Component {
  constructor() {
    this.state = {}
  }
  setState(state) {
    // console.log(state, '调用')
    update(this.$scope.$component,state)
  }
  _init(scope) {
    console.log('scope', scope)
    this.$scope = scope
  }
  createData(){
    this.__state = arguments[0]
    const text = this.state.count % 2 === 0 ? '偶数' : '奇数'
    Object.assign(this.__state,{
      text:text
    })
    return this.__state
  }
}

function update ($component,state={}) {
  $component.state = Object.assign($component.state,state)
  let data = $component.createData(state)
  $component.state = data
  data['$taroCompReady'] = true;
  $component.$scope.setData(data)
}

module.exports = {
  createPage,
  Component
}